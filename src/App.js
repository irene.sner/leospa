import React from 'react';
import Header from './components/Header'
import Section1 from './components/Section1'
import CardsContainer from './components/CardsContainer'
import PopularProcedures from './components/PopularProcedures'
import About from './components/About'
import Team from './components/Team'
import './App.css';

function App() {
  return (
    <div className="app">
     <Header />
     <Section1 />
     <CardsContainer />
     <PopularProcedures />
     <About />
     <Team />
    </div>
  );
}

export default App;
