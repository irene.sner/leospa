import React from 'react'
import Card from './Card.js'
import BodyTreatment from '../images/Body_Treatment.svg'
import FaceTreatment from '../images/Face_Treatment.svg'
import Massage from '../images/Massage.svg'
import HairRemoval from '../images/Hair_Removal.svg'
import './CardsContainer-xl.css'
// import './CardsContainer-xs.css'
// import './CardsContainer-sm.css'

function CardsContainer() {
    return(
        <div className='cards-container'>
            <Card icon={BodyTreatment} text={"Body Treatment"} />
            <Card icon={FaceTreatment} text={"Face Treatment"} />
            <Card icon={Massage} text={"Massage"} />
            <Card icon={HairRemoval} text={"Hair Removal"} />
        </div>
    )
}

export default CardsContainer