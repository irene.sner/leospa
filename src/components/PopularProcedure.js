import React from 'react'
import './PopularProcedure.css'

function PopularProcedure(props) {
    return(
        <div className="popular-procedure-wrapper">
            <div className="popular-procedure-img">
                
            </div>
            <div className="popular-procedure-info1">
                {props.procedureName}
            </div>
            <div className="popular-procedure-info2">
                {props.procedureDescription}
            </div>
            <div className="popular-procedure-button">Read More</div>
        </div>
    )
}

export default PopularProcedure