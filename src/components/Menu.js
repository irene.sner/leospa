import React from 'react'
import { useState } from 'react'
import Logo from "../images/Logo_Icon.png";
import OpenMenu from '../images/Menu.svg'
import CloseMenu from '../images/Close.svg'
import './Menu-xl.css'
// import './Menu-xs.css'
// import './Menu-sm.css'

function Menu() {
    const [showCloseMenu, setShowCloseMenu] = useState("inactive-close-menu")
    const [showMenu, setShowMenu] = useState("inactive-menu")
    return(
        <div className="menu-wrapper">
            <div className="logo">
                <img src={Logo} className="logo-img" alt="Leospa Logo" />
                <div className="logo-text">Leospa</div>
            </div>
            <div className={showMenu}>
                <ul>
                    <li>Home</li>
                    <li>About</li>
                    <li>Feature</li>
                    <li>Service</li>
                    <li>Contact</li>
                </ul>
            </div>
            <div>
                <img onClick={() => {
                    setShowMenu("active-menu")
                    setShowCloseMenu("active-close-menu")
                }
                } className="hamburger-menu" src={OpenMenu} alt="Open Menu" />
                <img onClick={() => {
                    setShowMenu("inactive-menu")
                    setShowCloseMenu("inactive-close-menu")
                }
                 } className={showCloseMenu} src={CloseMenu} alt="close-menu" />
            </div>
        </div>
    )
}

export default Menu