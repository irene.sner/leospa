import React from 'react'
import './Card-xl.css'
// import './Card-xs.css'
// import './Card-sm.css'

function Card(props) {
    return(
        <div className='card'>
           <img src={props.icon} alt={`${"" + props.icon}`}/>
           <div className="card-text">{props.text}</div>
        </div>
    )
}

export default Card