import React from "react";
import Menu from './Menu'
import "./Header-xl.css";
// import "./Header-xs.css"
// import './Header-sm.css'
import LeftBG from "../images/Left_BG_Flowers_Image.png";
import RightBG from "../images/Right_BG.png";
import Arrow from '../images/White_Arrow.png'
import Play_Icon from '../images/Play_Icon.png'

function Header() {
  return (
    <div className="header-wrapper">
      <img className="header-left-bg-img" src={LeftBG} alt="Floral Background" />
      <img className="header-right-bg-img" src={RightBG} alt="Pink Background" />
      <Menu />
      <div className="header-info-wrapper">
        <div className="header-info-1">spa {"&"} beauty center</div>
        <div className="header-info-2">Beauty and success starts here.</div>
        <div className="header-info-3">
            Together creeping heaven upon third dominion be upon won't 
            darkness rule behold it created good saw after she'd Our set living.
        </div>
        <button className="header-reserve-button">reserve now {<img src={Arrow} alt="White Arrow" />}</button>
        <div className="header-watch-button">
            <img src={Play_Icon} alt="Click to watch our story" />
            <div>Watch our story</div>
        </div>
      </div>
    </div>
  );
}

export default Header;
