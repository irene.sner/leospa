import React, { useState } from "react";
import "./AboutCarousel-xl.css";
import ExecutiveIcon1 from "../images/ExecutiveIcon1.png";

function AboutCarousel(props) {
  const inactiveDot = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="10px"
      height="10px"
    >
      <path
        fillRule="evenodd"
        fill="rgb(255, 195, 194)"
        d="M5.000,-0.000 C7.761,-0.000 10.000,2.238 10.000,5.000 C10.000,7.761 7.761,10.000 5.000,10.000 C2.239,10.000 -0.000,7.761 -0.000,5.000 C-0.000,2.238 2.239,-0.000 5.000,-0.000 Z"
      />
    </svg>
  );
  const activeDot = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="11px"
      height="11px"
    >
      <path
        fillRule="evenodd"
        fill="rgb(255, 129, 126)"
        d="M5.500,-0.000 C8.537,-0.000 11.000,2.462 11.000,5.500 C11.000,8.537 8.537,11.000 5.500,11.000 C2.462,11.000 -0.000,8.537 -0.000,5.500 C-0.000,2.462 2.462,-0.000 5.500,-0.000 Z"
      />
    </svg>
  );

  const [dots, handleDotClick] = useState([
    { id: 1, clicked: true },
    { id: 2, clicked: false },
    { id: 3, clicked: false },
    { id: 4, clicked: false },
  ]);
  const [carouselStatus, handleCarousel] = useState(false);
  const [slideNumber, changeSlide] = useState("");

  const delayAndStop = (slideNumber) => {
    handleCarousel(false);
    changeSlide(slideNumber);
  };

  return (
    <div className="about-carousel-wrapper">
      <div
        className={`${
          carouselStatus
            ? "sliders-container-active"
            : "sliders-container-delayed"
        } ${slideNumber}`}
        id="slides"
      >
        <div className="slide-wrapper">
          <div className="slide-info">{props.slide1Info}</div>
          <img
            className="executive-avatar"
            src={ExecutiveIcon1}
            alt="Executive"
          />
          <div className="executive-name">
            Jack Marsh, <span>Executive</span>
          </div>
        </div>
        <div className=" inactive-slide">
          <div className="slide-info">{props.slide2Info}</div>
        </div>
        <div className=" inactive-slide">
          <div className="slide-info">{props.slide3Info}</div>
        </div>
        <div className="inactive-slide">
          <div className="slide-info">{props.slide4Info}</div>
        </div>
      </div>
      <div className="control-dots-wrapper">
        <div className="control-dots">
          <div
            onClick={() => {
              if (dots[0].clicked === false) handleCarousel(true);
              handleDotClick([
                { id: 1, clicked: true },
                { id: 2, clicked: false },
                { id: 3, clicked: false },
                { id: 4, clicked: false },
              ]);
              delayAndStop("first-slide");
            }}
          >
            {dots[0].clicked ? activeDot : inactiveDot}
          </div>
          <div
            onClick={() => {
              if (dots[1].clicked === false) handleCarousel(true);
              handleDotClick([
                { id: 1, clicked: false },
                { id: 2, clicked: true },
                { id: 3, clicked: false },
                { id: 4, clicked: false },
              ]);
              delayAndStop("second-slide");
            }}
          >
            {dots[1].clicked ? activeDot : inactiveDot}
          </div>
          <div
            onClick={() => {
              if (dots[2].clicked === false) handleCarousel(true);
              handleDotClick([
                { id: 1, clicked: false },
                { id: 2, clicked: false },
                { id: 3, clicked: true },
                { id: 4, clicked: false },
              ]);
              delayAndStop("third-slide");
            }}
          >
            {dots[2].clicked ? activeDot : inactiveDot}
          </div>
          <div
            onClick={() => {
              if (!dots[3].clicked) handleCarousel(true);
              handleDotClick([
                { id: 1, clicked: false },
                { id: 2, clicked: false },
                { id: 3, clicked: false },
                { id: 4, clicked: true },
              ]);
              delayAndStop("fourth-slide");
            }}
          >
            {dots[3].clicked ? activeDot : inactiveDot}
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutCarousel;
