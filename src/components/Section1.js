import React from 'react'
import './Section1-xl.css'
// import './Section1-xs.css'
// import './Section1-sm.css'
import RedFlower from '../images/Red_Flower.png'
import PinkFlower from '../images/Pink_Flower.png' 
import Icon from '../images/Icon.png'

function Section1() {
    return(
        <div className="section-1-wrapper">
            <img className='red-flower-BG' src={RedFlower} alt='Red Flower Background' />
            <img className='pink-flower-BG' src={PinkFlower} alt='Pink Flower Background' />
            <div className='centered-elements'>
                <img className='icon' src={Icon} alt="Leospa Logo Icon"/>
                <div className='info-1'>About our spa center</div>
                <div className='info-2'>
                    Come and you will be Inspired!
                </div>
                <div className='info-3'>
                    It's the end of summer the sweltering heat makes human sweat
                    in the night and makes the plants and trees wilt even in the moonlit
                    nights. The eastern wing breeze brings an eerie feeling, that the monsoon clouds
                    are soon coming, there is a strange silence in the ears, the sky gets darker and darker
                </div>
                <button className='read-more-button'>Read more</button>
            </div>
        </div>
    )
}

export default Section1